//
//  OTPMaterialSwitch+Rx.swift
//  rxcocoaws
//
//  Created by Nagy Ádám on 2018. 11. 29..
//  Copyright © 2018. Edem. All rights reserved.
//

import RxSwift
import RxCocoa

extension Reactive where Base: OTPMaterialSwitch {
    var delegate: DelegateProxy<OTPMaterialSwitch, OTPMaterialSwitchDelegate> {
        return RxOTPSwitchDelegateProxy.proxy(for: base)
    }

    var value: Observable<Bool> {
        let proxy = RxOTPSwitchDelegateProxy.proxy(for: base)
        return proxy.value
    }

    var state: Observable<OTPMaterialSwitchState> {
        let proxy = RxOTPSwitchDelegateProxy.proxy(for: base)
        return proxy.state
    }
}
