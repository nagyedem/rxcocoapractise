//
//  OTPMaterialSwitch.swift
//  OTPMaterialSwitch
//

import UIKit

// MARK: - Switch type
public enum OTPMaterialSwitchStyle {
    case light, dark, medium
}

// MARK: - Initial state (on or off)
public enum OTPMaterialSwitchState {
    case on, off
}

// MARK: - Initial OTPMaterialSwitch size (big, normal, small)
public enum OTPMaterialSwitchSize {
    case big, normal, small
}

public protocol OTPMaterialSwitchDelegate: class {
    func switchStateChanged(_ currentState: OTPMaterialSwitchState)
    func valueChanged(_ enabled: Bool)
}

public class OTPMaterialSwitch: UIControl {

    public weak var delegate: OTPMaterialSwitchDelegate?
    var isOn: Bool = false

    var isBounceEnabled: Bool = false {
        didSet {
            // Set bounce value, 3.0 if enabled and none for disabled
            if self.isBounceEnabled {
                self.bounceOffset = 3.0
            } else {
                self.bounceOffset = 0.0
            }
        }
    }

    var isRippleEnabled: Bool = false
    var thumbOnTintColor: UIColor!
    var thumbOffTintColor: UIColor!
    var trackOnTintColor: UIColor!
    var trackOffTintColor: UIColor!
    var thumbDisabledTintColor: UIColor!
    var trackDisabledTintColor: UIColor!
    var rippleFillColor: UIColor = .gray
    var switchThumb: UIButton!
    var track: UIView!
    var trackThickness: CGFloat!
    var thumbSize: CGFloat!

    private var thumbOnPosition: CGFloat!
    private var thumbOffPosition: CGFloat!
    private var bounceOffset: CGFloat!
    private var thumbStyle: OTPMaterialSwitchStyle!
    private var rippleLayer: CAShapeLayer!

    public override var accessibilityIdentifier: String? {
        didSet {
            switchThumb.accessibilityIdentifier = accessibilityIdentifier
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // swiftlint:disable function_body_length
    convenience init(withSize size: OTPMaterialSwitchSize,
                     thumbOnTintColor: UIColor,
                     thumbOffTintColor: UIColor,
                     trackOnTintColor: UIColor,
                     trackOffTintColor: UIColor,
                     thumbDisabledTintColor: UIColor,
                     trackDisabledTintColor: UIColor) {
        var frame: CGRect!
        var trackFrame = CGRect.zero
        var thumbFrame = CGRect.zero

        // Determine switch size
        // Actual initialization with selected size
        switch size {
        case .big:
            frame = CGRect(x: 0, y: 0, width: 50, height: 40)
            self.init(frame: frame)
            self.trackThickness = 23.0
            self.thumbSize = 31.0

        case .normal:
            frame = CGRect(x: 0, y: 0, width: 40, height: 30)
            self.init(frame: frame)
            self.trackThickness = 17.0
            self.thumbSize = 24.0

        case .small:
            frame = CGRect(x: 0, y: 0, width: 30, height: 25)
            self.init(frame: frame)
            self.trackThickness = 13.0
            self.thumbSize = 18.0
        }

        self.thumbOnTintColor = thumbOnTintColor
        self.thumbOffTintColor = thumbOffTintColor
        self.trackOnTintColor = trackOnTintColor
        self.trackOffTintColor = trackOffTintColor
        self.thumbDisabledTintColor = thumbDisabledTintColor
        self.trackDisabledTintColor = trackDisabledTintColor

        self.isEnabled = true

        self.isRippleEnabled = true
        self.isBounceEnabled = true

        self.bounceOffset = 3.0

        trackFrame.size.height = self.trackThickness
        trackFrame.size.width = frame.size.width
        trackFrame.origin.x = 0.0
        trackFrame.origin.y = (frame.size.height - trackFrame.size.height) / 2
        thumbFrame.size.height = self.thumbSize
        thumbFrame.size.width = thumbFrame.size.height
        thumbFrame.origin.x = 0.0
        thumbFrame.origin.y = (frame.size.height - thumbFrame.size.height) / 2

        self.track = UIView(frame: trackFrame)
        self.track.backgroundColor = .gray
        self.track.layer.cornerRadius = min(self.track.frame.size.height, self.track.frame.size.width) / 2
        self.addSubview(self.track)

        self.switchThumb = UIButton(frame: thumbFrame)
        self.switchThumb.backgroundColor = .white
        self.switchThumb.layer.cornerRadius = self.switchThumb.frame.size.height / 2
        self.switchThumb.layer.shadowOpacity = 0.5
        self.switchThumb.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)

        self.switchThumb.layer.shadowColor = UIColor.black.cgColor
        self.switchThumb.layer.shadowRadius = 2.0

        // Add events for user action
        self.switchThumb.addTarget(self,
                                   action: #selector(self.onTouchDown(btn:withEvent:)),
                                   for: UIControl.Event.touchDown)
        self.switchThumb.addTarget(self,
                                   action: #selector(onTouchUpOutsideOrCanceled(btn:withEvent:)),
                                   for: UIControl.Event.touchUpOutside)
        self.switchThumb.addTarget(self,
                                   action: #selector(self.switchThumbTapped),
                                   for: UIControl.Event.touchUpInside)
        self.switchThumb.addTarget(self,
                                   action: #selector(self.onTouchDragInside(btn:withEvent:)),
                                   for: UIControl.Event.touchDragInside)
        self.switchThumb.addTarget(self,
                                   action: #selector(self.onTouchUpOutsideOrCanceled(btn:withEvent:)),
                                   for: UIControl.Event.touchCancel)

        self.addSubview(self.switchThumb)

        thumbOnPosition = self.frame.size.width - self.switchThumb.frame.size.width
        thumbOffPosition = self.switchThumb.frame.origin.x
    }
    // swiftlint:enable function_body_length

    func setInitial(state: Bool) {
        self.isOn = state
        // Set thumb's initial position from state property
        if state {
            self.switchThumb.backgroundColor = self.thumbOnTintColor
            var thumbFrame = self.switchThumb.frame
            thumbFrame.origin.x = self.thumbOnPosition
            self.switchThumb.frame = thumbFrame
        } else {
            self.switchThumb.backgroundColor = self.thumbOffTintColor

            var thumbFrame = self.switchThumb.frame
            thumbFrame.origin.x = self.thumbOffPosition
            self.switchThumb.frame = thumbFrame
        }

        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.switchAreaTapped(recognizer:)))
        self.addGestureRecognizer(singleTap)

        self.setOn(on: self.isOn, animated: self.isRippleEnabled, initial: true)
    }

    func getSwitchState() -> Bool {
        return self.isOn
    }

    func setOn(on: Bool, animated: Bool, initial: Bool = false) {
        if on {
            if animated {
                // set on with animation
                self.changeThumbStateONwithAnimation(initial: initial)
            } else {
                // set on without animation
                self.changeThumbStateONwithoutAnimation(initial: initial)
            }
        } else {
            if animated {
                // set off with animation
                self.changeThumbStateOFFwithAnimation(initial: initial)
            } else {
                // set off without animation
                self.changeThumbStateOFFwithoutAnimation(initial: initial)
            }
        }
    }

    func setEnabled(enabled: Bool) {
        super.isEnabled = enabled
        UIView.animate(withDuration: 0.1) {
            if enabled {
                if self.isOn {
                    self.switchThumb.backgroundColor = self.thumbOnTintColor
                    self.track.backgroundColor = self.trackOnTintColor
                } else {
                    self.switchThumb.backgroundColor = self.thumbOffTintColor
                    self.track.backgroundColor = self.trackOffTintColor
                }
            } else { // if disabled
                self.switchThumb.backgroundColor = self.thumbDisabledTintColor
                self.track.backgroundColor = self.trackDisabledTintColor
            }
            self.isEnabled = enabled
        }
    }

    @objc func switchAreaTapped(recognizer: UITapGestureRecognizer) {
        self.changeThumbState()
    }

    func changeThumbState() {
        if self.isOn {
            self.changeThumbStateOFFwithAnimation()
        } else {
            self.changeThumbStateONwithAnimation()
        }

        if self.isRippleEnabled {
            self.animateRippleEffect()
        }
    }

    func changeThumbStateONwithAnimation(initial: Bool = false) {
        // switch movement animation
        UIView.animate(withDuration: 0.15, delay: 0.05, options: UIView.AnimationOptions.curveEaseInOut, animations: {

            var thumbFrame = self.switchThumb.frame
            thumbFrame.origin.x = self.thumbOnPosition + self.bounceOffset
            self.switchThumb.frame = thumbFrame
            if self.isEnabled {
                self.switchThumb.backgroundColor = self.thumbOnTintColor
                self.track.backgroundColor = self.trackOnTintColor
            } else {
                self.switchThumb.backgroundColor = self.thumbDisabledTintColor
                self.track.backgroundColor = self.trackDisabledTintColor
            }
            self.isUserInteractionEnabled = false

        }, completion: { _  in

            // change state to ON
            if !self.isOn {
                self.isOn = true // Expressly put this code here to change surely and send action correctly
                self.sendActions(for: UIControl.Event.valueChanged)
                self.delegate?.switchStateChanged(.on)
            }
            self.isOn = true

            UIView.animate(withDuration: 0.15, animations: {
                // Bounce to the position
                var thumbFrame = self.switchThumb.frame
                thumbFrame.origin.x = self.thumbOnPosition
                self.switchThumb.frame = thumbFrame

            }, completion: { _ in
                self.isUserInteractionEnabled = true
                if !initial {
                    self.delegate?.valueChanged(self.isOn)
                }
            })
        })
    }

    func changeThumbStateOFFwithAnimation(initial: Bool = false) {
        // switch movement animation
        UIView.animate(withDuration: 0.15, delay: 0.05, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            var thumbFrame = self.switchThumb.frame
            thumbFrame.origin.x = self.thumbOffPosition - self.bounceOffset
            self.switchThumb.frame = thumbFrame
            if self.isEnabled {
                self.switchThumb.backgroundColor = self.thumbOffTintColor
                self.track.backgroundColor = self.trackOffTintColor
            } else {
                self.switchThumb.backgroundColor = self.thumbDisabledTintColor
                self.track.backgroundColor = self.trackDisabledTintColor
            }
            self.isUserInteractionEnabled = false

        }, completion: { _ in

            // change state to OFF
            if self.isOn {
                self.isOn = false // Expressly put this code here to change surely and send action correctly
                self.sendActions(for: UIControl.Event.valueChanged)
                self.delegate?.switchStateChanged(.off)
            }
            self.isOn = false
            // Log.debug("now isOn: %d", self.isOn)
            // Log.debug("thumb end pos: %@", NSStringFromCGRect(self.switchThumb.frame))
            // Bouncing effect: Move thumb a bit, for better UX

            UIView.animate(withDuration: 0.15, animations: {

                // Bounce to the position
                var thumbFrame = self.switchThumb.frame
                thumbFrame.origin.x = self.thumbOffPosition
                self.switchThumb.frame = thumbFrame
            }, completion: { _ in
                self.isUserInteractionEnabled = true
                if !initial {
                    self.delegate?.valueChanged(self.isOn)
                }
            })
        })
    }

    // Without animation
    func changeThumbStateONwithoutAnimation(initial: Bool = false) {

        var thumbFrame = self.switchThumb.frame
        thumbFrame.origin.x = thumbOnPosition
        self.switchThumb.frame = thumbFrame
        if self.isEnabled {
            self.switchThumb.backgroundColor = self.thumbOnTintColor
            self.track.backgroundColor = self.trackOnTintColor
        } else {
            self.switchThumb.backgroundColor = self.thumbDisabledTintColor
            self.track.backgroundColor = self.trackDisabledTintColor
        }

        if !self.isOn {
            self.isOn = true
            self.sendActions(for: UIControl.Event.valueChanged)
            self.delegate?.switchStateChanged(.on)
        }
        self.isOn = true
        if !initial {
            self.delegate?.valueChanged(self.isOn)
        }
    }

    // Without animation
    func changeThumbStateOFFwithoutAnimation(initial: Bool = false) {

        var thumbFrame = self.switchThumb.frame
        thumbFrame.origin.x = thumbOffPosition
        self.switchThumb.frame = thumbFrame

        if self.isEnabled {
            self.switchThumb.backgroundColor = self.thumbOffTintColor
            self.track.backgroundColor = self.trackOffTintColor
        } else {
            self.switchThumb.backgroundColor = self.thumbDisabledTintColor
            self.track.backgroundColor = self.trackDisabledTintColor
        }

        if self.isOn {
            self.isOn = false
            self.sendActions(for: UIControl.Event.valueChanged)
            self.delegate?.switchStateChanged(.off)
        }
        self.isOn = false
        if !initial {
            self.delegate?.valueChanged(self.isOn)
        }
    }

    // Initialize and appear ripple effect
    func initializeRipple() {
        // Ripple size is twice as large as switch thumb

        let rippleScale: CGFloat = 2.0
        var rippleFrame = CGRect.zero
        rippleFrame.origin.x = -self.switchThumb.frame.size.width / (rippleScale * 2)
        rippleFrame.origin.y = -self.switchThumb.frame.size.height / (rippleScale * 2)
        rippleFrame.size.height = self.switchThumb.frame.size.height * rippleScale
        rippleFrame.size.width = rippleFrame.size.height

        let path = UIBezierPath(roundedRect: rippleFrame, cornerRadius: self.switchThumb.layer.cornerRadius * 2)

        // Set ripple layer attributes
        rippleLayer = CAShapeLayer()
        rippleLayer.path = path.cgPath
        rippleLayer.frame = rippleFrame
        rippleLayer.opacity = 0.2
        rippleLayer.strokeColor = UIColor.clear.cgColor
        rippleLayer.fillColor = self.rippleFillColor.cgColor
        rippleLayer.lineWidth = 0

        self.switchThumb.layer.insertSublayer(rippleLayer, below: self.switchThumb.layer)
    }

    func animateRippleEffect() {
        // Create ripple layer
        if rippleLayer == nil {
            self.initializeRipple()
        }

        // Animation begins from here
        rippleLayer.opacity = 0.0

        CATransaction.begin()

        //remove layer after animation completed
        CATransaction.setCompletionBlock {
            self.rippleLayer.removeFromSuperlayer()
            self.rippleLayer = nil
        }

        // Scale ripple to the modelate size
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 0.5
        scaleAnimation.toValue = 1.25

        // Alpha animation for smooth disappearing
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
        alphaAnimation.fromValue = 0.4
        alphaAnimation.toValue = 0

        // Do above animations at the same time with proper duration
        let animation = CAAnimationGroup()
        animation.animations = [scaleAnimation, alphaAnimation]
        animation.duration = 0.4
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        rippleLayer.add(animation, forKey: nil)

        CATransaction.commit()
    }

    @objc func onTouchDown(btn: UIButton, withEvent event: UIEvent) {

        // Log.debug("touchDown called")
        if self.isRippleEnabled {
            self.initializeRipple()

            // Animate for appearing ripple circle when tap and hold the switch thumb
            CATransaction.begin()

            let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.fromValue = 0.0
            scaleAnimation.toValue = 1.0

            let alphaAnimation = CABasicAnimation(keyPath: "opacity")
            alphaAnimation.fromValue = 0
            alphaAnimation.toValue = 0.2

            // Do above animations at the same time with proper duration
            let animation = CAAnimationGroup()
            animation.animations = [scaleAnimation, alphaAnimation]
            animation.duration = 0.4
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            rippleLayer.add(animation, forKey: nil)
            CATransaction.commit()
        }
    }

    @objc func switchThumbTapped() {
        self.changeThumbState()
    }

    @objc func onTouchUpOutsideOrCanceled(btn: UIButton, withEvent event: UIEvent) {

        // Log.debug("Touch released at ouside")
        if let touch = event.touches(for: btn)?.first {

            let prevPos = touch.previousLocation(in: btn)
            let pos = touch.location(in: btn)

            let dX = pos.x - prevPos.x

            //Get the new origin after this motion
            let newXOrigin = btn.frame.origin.x + dX
            //Log.debug("Released tap X pos: %f", newXOrigin)

            if newXOrigin > (self.frame.size.width - self.switchThumb.frame.size.width) / 2 {
                //Log.debug("thumb pos should be set *ON*")
                self.changeThumbStateONwithAnimation()
            } else {
                //Log.debug("thumb pos should be set *OFF*")
                self.changeThumbStateOFFwithAnimation()
            }

            if self.isRippleEnabled {
                self.animateRippleEffect()
            }
        }
    }

    // Drag the switch thumb
    @objc func onTouchDragInside(btn: UIButton, withEvent event: UIEvent) {
        //This code can go awry if there is more than one finger on the screen

        if let touch = event.touches(for: btn)?.first {

            let prevPos = touch.previousLocation(in: btn)
            let pos = touch.location(in: btn)
            let dX = pos.x - prevPos.x

            //Get the original position of the thumb
            var thumbFrame = btn.frame

            thumbFrame.origin.x += dX
            //Make sure it's within two bounds
            thumbFrame.origin.x = min(thumbFrame.origin.x, thumbOnPosition)
            thumbFrame.origin.x = max(thumbFrame.origin.x, thumbOffPosition)

            //Set the thumb's new frame if need to
            if thumbFrame.origin.x != btn.frame.origin.x {
                btn.frame = thumbFrame
            }
        }
    }
}
